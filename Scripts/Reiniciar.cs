﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class Reiniciar : MonoBehaviour {

	public string scene;

	void OnTriggerEnter(Collider other){

		if (other.gameObject.CompareTag ("Player")) {
			WriteString ("Se cayó");
			SceneManager.LoadScene (scene);
		}

	}

	public void WriteString(string resultado){
		string path="Assets/Text/test4.txt";
		StreamWriter writer=new StreamWriter(path,true);
		writer.WriteLine(resultado);
		writer.Close();

	}
}
