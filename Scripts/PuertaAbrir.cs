﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuertaAbrir : MonoBehaviour {

	public GameObject puerta;
	public Text texto;
	private int vel=90;
	//private float angulo;
	private Vector3 direccion;
	public GameObject otraOpcion;
	public GameObject otraOpcion2;

	//void Start(){
	//	angulo = transform.eulerAngles.y;
	//}
	// Use this for initialization
	void OnTriggerStay(Collider other) {
		if (other.tag == "Player") {
			//Debug.Log ("entro");
			if (texto) {
				texto.enabled = true;
			}
			if (Input.GetKeyDown ("o")) {
				//angulo = 90;
				direccion = Vector3.up;
				puerta.transform.Rotate (direccion * vel);

				//Debug.Log(Mathf.Round (puerta.transform.eulerAngles.y));
				//if (Mathf.Round (puerta.transform.eulerAngles.y) != 0) {
				//	puerta.transform.Rotate (direccion * vel);
				//}
				if (otraOpcion) {
					if (otraOpcion.activeSelf == true) {
						otraOpcion.SetActive (false);
					}
				}
				if (otraOpcion2) {
					if (otraOpcion2.activeSelf == true) {
						otraOpcion2.SetActive (false);
					}
				}
				gameObject.SetActive(false);
				if (texto) {
					texto.enabled=false;
				}
			}
			
			//	Debug.Log ("abre");
				//puerta.transform.rotation = Quaternion.Lerp (puerta.transform.rotation, Quaternion.Euler (0, (-120), 0), 30 * Time.deltaTime);
				//puerta.transform.Rotate(Vector3.up,500*Time.deltaTime);
			
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.tag == "Player") {
			if (texto) {
				texto.enabled=false;
			}
		}
	}
}
