﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;

public class QuieroSalir : MonoBehaviour
{
	// 200x300 px window will apear in the center of the screen.
	private Rect windowRect = new Rect ((Screen.width - 200)/2, (Screen.height - 200)/2, 200, 200);
	// Only show it if needed.
	private bool show = false;
	public string nuevaEscena;
	//public GameObject pibe;
	//public Camera cam;

	void OnTriggerEnter (Collider other) 
	{
		if (other.tag == "Player") {
			Open ();
		}
	}

	void OnGUI () 
	{
		if(show)
			windowRect = GUI.Window (0, windowRect, DialogWindow, "¿Querés salir?");
	}	

	// This is the actual window.
	void DialogWindow (int windowID)
	{

		if(GUI.Button(new Rect(5,60, windowRect.width - 10, 20), "Sí"))
		{
			WriteString ("Quiso salir");
			SceneManager.LoadScene (nuevaEscena);
			show = false;
		}

		if(GUI.Button(new Rect(5,90, windowRect.width - 10, 20), "No"))
		{
			//Application.Quit();
			show = false;
		}
	}

	// To open the dialogue from outside of the script.
	public void Open()
	{
		show = true;
	}

	public void WriteString(string resultado){
		string path="Assets/Text/test4.txt";
		StreamWriter writer=new StreamWriter(path,true);
		writer.WriteLine(resultado);
		writer.Close();

	}
}
