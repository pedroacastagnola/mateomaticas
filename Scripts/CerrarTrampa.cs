﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CerrarTrampa : MonoBehaviour {

	public GameObject trampa;
	private Vector3 direccion;
	private int vel=90;
	public GameObject opcion1;
	public GameObject opcion2;
	public GameObject opcion3;
	public GameObject entrada;
	private int random;
	private string elegida;
	public Text texto1;
	public Text texto2;
	public Text texto3;
	public Text pregunta;

	void GenerarPregunta(){
		string[] uno=new string[4];
		string[] dos=new string[4] ;
		string[] tres=new string[4];

		if (this.tag == "NivelBajo") {
			uno [0] = "El número 51 es múltiplo de...";uno [1] = "11";uno [2] =  "17";uno[3]= "13" ;
			dos [0] = "Número par divisor de 28";dos [1] = "6";dos [2] = "4";dos[3]= "8" ;
			tres [0] = "El número 121 es múltiplo de...";tres [1] = "17";tres [2] = "11";tres[3]= "7" ;
		}

		string[] opciones = { "1", "2", "3" };
		random = Random.Range (0, 3);
		elegida = opciones [random];

		if (elegida == "1") {
			pregunta.text = uno [0];
			texto1.text = uno [1];
			texto2.text = uno [2];
			texto3.text = uno [3];


		}

		if (elegida == "2") {
			pregunta.text = dos [0];
			texto1.text = dos [1];
			texto2.text = dos [2];
			texto3.text = dos [3];

		}
		if (elegida == "3") {

			pregunta.text = tres [0];
			texto1.text = tres [1];
			texto2.text = tres [2];
			texto3.text = tres [3];

		}

	}
	void OnTriggerEnter(Collider other){
		GenerarPregunta ();
		if (other.tag == "Player") {
			direccion = Vector3.forward;
			//Debug.Log (Mathf.Round (puerta.transform.eulerAngles.y));
			//if (Mathf.Round (puerta.transform.eulerAngles.y) != 90) {
			trampa.transform.Rotate (direccion * vel);
		}
		if (opcion1) {
			if (opcion1.activeSelf == false) {
				opcion1.SetActive (true);
				for (int i = 0; i < opcion1.transform.childCount; ++i)
				{
					opcion1.transform.GetChild(i).gameObject.SetActive(true);
				}
			}
		}
		if (opcion2) {
			if (opcion2.activeSelf == false) {
				opcion2.SetActive (true);
				for (int i = 0; i < opcion2.transform.childCount; ++i)
				{
					opcion2.transform.GetChild(i).gameObject.SetActive(true);
				}
			}
		}
		if (opcion3) {
			if (opcion3.activeSelf == false) {
				opcion3.SetActive (true);
				for (int i = 0; i < opcion3.transform.childCount; ++i)
				{
					opcion3.transform.GetChild(i).gameObject.SetActive(true);
				}
			}
		}
		if (entrada.activeSelf == false) {
			entrada.SetActive (true);
		}
	}
}
