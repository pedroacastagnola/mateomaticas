﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbrirTrampa : MonoBehaviour {

	public GameObject puerta;
	public Text texto;
	public int vel;
	private Vector3 direccion;

	void OnTriggerStay(Collider other) {
		if (other.tag == "Player") {
			//Debug.Log ("entro");
			texto.enabled=true;
			if (Input.GetKeyDown ("o")) {
				//angulo = 90;
				direccion = Vector3.back;
				puerta.transform.Rotate (direccion * vel);

				gameObject.SetActive(false);
				texto.enabled=false;
			}

		}
	}

	void OnTriggerExit(Collider other) {
		if (other.tag == "Player") {
			//pistaMostrada=false;
			texto.enabled=false;
		}
	}
}
