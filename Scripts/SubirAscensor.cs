﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SubirAscensor : MonoBehaviour {

	public GameObject ascensor;
	public GameObject puerta;
	private bool seMueve=false;
	private bool sube=false;
	public string nuevaEscena;

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			seMueve = true;
			}
	}

	void Update(){
		if (seMueve == true) {
			puerta.transform.Translate (Vector3.left * Time.deltaTime );

			//Debug.Log (seMueve);
			//Debug.Log (puerta.transform.position.z);
		}

		if(puerta.transform.position.z>45.05){
			//Debug.Log (seMueve);
			//Debug.Log (puerta.transform.position.z);
			seMueve=false;
			sube = true;
		}

		if (sube) {
			//Debug.Log (puerta.transform.position);
			ascensor.transform.Translate (Vector3.up * Time.deltaTime);
		}

		if (ascensor.transform.position.y > 6) {
			SceneManager.LoadScene (nuevaEscena);
		}

	}
}
