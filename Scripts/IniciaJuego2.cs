﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IniciaJuego2 : MonoBehaviour {

	private Rect windowRect = new Rect ((Screen.width - 250)/2, (Screen.height - 200)/2, 250, 200);
	private bool show = false;
	// Use this for initialization
	void Start () {
		Open ();
		
	}
	

	void OnGUI () 
	{
		if(show)
			windowRect = GUI.Window (0, windowRect, DialogWindow, "Formá la ecuación");
	}	

	// This is the actual window.
	void DialogWindow (int windowID)
	{
		GUI.Label (new Rect (5, 40, windowRect.width - 10, 100), "Seguí el orden de las pistas de menor a mayor para formar el lenguaje coloquial de la ecuación. ¡Atención! Tenés que resolverla. Cuando lo hagas, buscá dónde escribir la respuesta y encontrá la salida.");

		if(GUI.Button(new Rect(10,150, windowRect.width - 20, 30), "¡Jugar!"))
		{
			show = false;
		}

	}

	// To open the dialogue from outside of the script.
	public void Open()
	{
		show = true;
	}
}
