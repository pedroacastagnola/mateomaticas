﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;
using System.IO;

public class Salida : MonoBehaviour {

	public GameObject aDesaparecer;
	public Text texto;
	public Canvas cuadro;
	public InputField numero;
	public GameObject pared;
	private bool seMueve=false;
	//private string n="";

	void OnTriggerStay(Collider other) {
		if (other.tag == "Player") {
			//pistaMostrada = true;
			texto.enabled=true;
			if (Input.GetKeyDown ("r")) {
				//Debug.Log ("Escribe el resultado");
				cuadro.enabled = true;
			}
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.tag == "Player") {
			//pistaMostrada=false;
			texto.enabled=false;
		}
	}

	public void AceptarNumero(){
		seMueve = true;
		aDesaparecer.SetActive(false);
		WriteString (numero.text);

	}

	public void WriteString(string resultado){
		string path="Assets/Text/test3.txt";
		StreamWriter writer=new StreamWriter(path,true);
		writer.WriteLine(resultado);
		writer.Close();
			
	}

	void Update(){
		if (seMueve == true) {
			pared.transform.Translate (Vector3.left * Time.deltaTime * 2);
		}

		if(pared.transform.position.x<-6.10){
			seMueve=false;
		}

	}
}