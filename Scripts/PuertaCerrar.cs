﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;
using System.IO;

public class PuertaCerrar : MonoBehaviour {


	public GameObject pared;
	//public GameObject pared2;
	public GameObject puerta;
	private Vector3 direccion;
	private int vel=90;
	public GameObject antorcha;
	public GameObject cubo;
	public Text pregunta;
	public Text respuesta;
	//public GameObject opcion;

	public void WriteString(string resultado){
		string path="Assets/Text/test4.txt";
		StreamWriter writer=new StreamWriter(path,true);
		writer.WriteLine(resultado);
		writer.Close();

	}

	void OnTriggerEnter(Collider other) {

		if (other.tag == "Player") {
			direccion = -Vector3.up;
			//Debug.Log (Mathf.Round (puerta.transform.eulerAngles.y));
			//if (Mathf.Round (puerta.transform.eulerAngles.y) != 90) {
			puerta.transform.Rotate (direccion * vel);
			if (pregunta && respuesta) {
				WriteString (pregunta.text + ": " + respuesta.text);
			}
			//	}
			if (pared) {
				if (pared.activeSelf == true) {
					pared.SetActive (false);
				}
			}
				

			if (antorcha) {
				antorcha.transform.Rotate (Vector3.left * 45);
			}

			if (cubo) {
				if (cubo.activeSelf == false) {
					cubo.SetActive (true);
				}
			}

			gameObject.SetActive (false);
			this.transform.parent.gameObject.SetActive (false);
			/*if (opcion) {
				opcion.SetActive (false);
			}*/
			/*if(pared2.activeSelf==true){
				pared2.SetActive (false);
			}*/
			//gameObject.SetActive(false);
			}

			//	Debug.Log ("abre");
			//puerta.transform.rotation = Quaternion.Lerp (puerta.transform.rotation, Quaternion.Euler (0, (-120), 0), 30 * Time.deltaTime);
			//puerta.transform.Rotate(Vector3.up,500*Time.deltaTime);

		}
	}
