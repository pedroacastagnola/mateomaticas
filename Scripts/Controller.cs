﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Controller : MonoBehaviour {

	public Text texto1;
	public Text texto2;
	public Text texto3;
	public Text texto4;
	public Text texto5;
	private int random;

	void Start(){
		string[] uno={"La quinta parte del doble de un n° es igual a la raíz cúbica de sesenta y cuatro",
			"La quinta parte", "del doble de un n°","es igual a","la raíz cúbica de","sesenta y cuatro"};
		string[] dos={"El producto de la raíz cuadrada de 100 y cierto n° es menor que el producto del triple de diez y la raíz cuadrada de cuatro",
			"El producto de la raíz cuadrada de 100", "y cierto n°","es menor que","el producto del triple de diez","y la raíz cuadrada de cuatro"};
		random = Random.Range (0, 2);
		if (random == 1) {
			texto1.text = uno [1];
			texto2.text = uno [2];
			texto3.text = uno [3];
			texto4.text = uno [4];
			texto5.text = uno [5];
			WriteString (uno [0]);
		} else {
			texto1.text = dos [1];
			texto2.text = dos [2];
			texto3.text = dos [3];
			texto4.text = dos [4];
			texto5.text = dos [5];
			WriteString (dos [0]);
		}
			
	}

	public void WriteString(string resultado){
		string path="Assets/Text/test3.txt";
		StreamWriter writer=new StreamWriter(path,true);
		writer.WriteLine(resultado);
		writer.Close();

	}
		
}
