﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AparecePista : MonoBehaviour {

	//private boolean pistaMostrada=false;
	public Text texto;
	//public Text	consigna;

	void OnTriggerStay(Collider other) {
		if (other.tag == "Player") {
			//pistaMostrada = true;
			//Debug.Log(texto.text);
			texto.enabled=true;
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.tag == "Player") {
			//pistaMostrada=false;
			texto.enabled=false;
		}
	}
}
