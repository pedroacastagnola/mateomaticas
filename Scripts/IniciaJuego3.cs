﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IniciaJuego3 : MonoBehaviour {


	private Rect windowRect = new Rect ((Screen.width - 250)/2, (Screen.height - 200)/2, 250, 200);
	private bool show = false;
	// Use this for initialization
	void Start () {
		Open ();

	}


	void OnGUI () 
	{
		if(show)
			windowRect = GUI.Window (0, windowRect, DialogWindow, "El Laberinto");
	}	

	// This is the actual window.
	void DialogWindow (int windowID)
	{
		GUI.Label (new Rect (5, 40, windowRect.width - 10, 100), "Movete a través del laberinto de múltiplos y divisores, abriendo las puertas correctas según la consigna.");

		if(GUI.Button(new Rect(10,150, windowRect.width - 20, 30), "¡Jugar!"))
		{
			show = false;
		}

	}

	// To open the dialogue from outside of the script.
	public void Open()
	{
		show = true;
	}
}

