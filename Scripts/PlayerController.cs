﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;
using System.IO;

public class PlayerController : MonoBehaviour {

	public Text goalText;
//	public Text countText;
//	public Text winText; 
	public GameObject piso;

	private string[] goals;
	private string goal;
	private int random;
	private long count;
	private int multiply;
	private Rect windowRect = new Rect ((Screen.width - 250)/2, (Screen.height - 200)/2, 250, 200);
	private bool show = false;
	private bool show2 = false;

	void Start(){
		Open2();
		count = 0;
		multiply = 0;
		goals = new string[6];
		goals[0] = "824.136.294";
		goals[1] = "3.983.147.062";
		goals[2] = "2.104.200.580";
		goals[3] = "7.100.499.806";
		goals[4] = "6.477.000.000";
		goals[5] = "6.210.000.787";

		random = Random.Range (0, 6);
		goal = goals [random];
			
//		SetCountText ();
		SetGoalText ();
//		winText.text = "";

	}


	void FixedUpdate(){

	}

	void OnGUI () 
	{
		if (show) {
			windowRect = GUI.Window (0, windowRect, DialogWindow, "Formá el número");
		}
		if (show2) {
			//var estilo = GUI.skin.GetStyle ("Window");
			//estilo.normal.background.
			GUI.backgroundColor=Color.green;
			//windowRect = new Rect ((Screen.width - 400)/2, (Screen.height - 100)/2, 400, 100);
			windowRect = GUI.Window (0, windowRect, DialogWindow2, "¿Cómo juego?");

		}
	}	

	// This is the actual window.
	void DialogWindow (int windowID)
	{
		GUI.Label (new Rect (5, 40, windowRect.width - 10, 100), "Componé utilizando las unidades de divesos órdenes para llegar al número que cumpla con el objetivo.¡Animate y usá tu creatividad! Hay varias formas de componerlo.");

		if(GUI.Button(new Rect(10,150, windowRect.width - 20, 30), "¡Jugar!"))
		{
			show = false;

		}

	}

	void DialogWindow2 (int windowID)
	{
		//		var estilo = GUI.skin.GetStyle ("Label");
		//		estilo.fontSize=1;

		GUI.Label (new Rect (5, 40, windowRect.width - 10, 100), "Para moverte usá las flechas del teclado, la barra espaciadora para saltar y la 'o' para abrir las puertas.");

		GUI.backgroundColor = Color.green;

		if(GUI.Button(new Rect(10,150, windowRect.width - 20, 30), "Aceptar"))
		{
			show2 = false;
			Open ();
		}

	}

	// To open the dialogue from outside of the script.
	public void Open()
	{
		show = true;
	}

	public void Open2()
	{
		show2 = true;
	}

	void OnTriggerEnter(Collider other){

		if (other.gameObject.CompareTag ("Fin")) {
			piso.SetActive (false);
			other.gameObject.SetActive (false);
//			winText.text = "Juego Terminado! La nota obtenida fue: " + getScore ()
//				+ ".";
			getScore();
		}

		if (other.gameObject.CompareTag ("Reinicio")) {
			transform.position = new Vector3(0.0f, 0.0f, 0.0f);
			WriteString ("Se cayó");
			Start();
		}



		if (other.gameObject.CompareTag ("Uno")) {

			StartCoroutine( ShowAndHide(other.gameObject, 20.0f) ); // 1 second
			count = count + (1*multiply);
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Dos")) {

			StartCoroutine( ShowAndHide(other.gameObject, 20.0f) ); // 1 second
			count = count + (2*multiply);
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Tres")) {

			StartCoroutine( ShowAndHide(other.gameObject, 20.0f) ); // 1 second
			count = count + (3*multiply);
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Cuatro")) {

			StartCoroutine( ShowAndHide(other.gameObject, 20.0f) ); // 1 second
			count = count + (4*multiply);
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Cinco")) {

			StartCoroutine( ShowAndHide(other.gameObject, 20.0f) ); // 1 second
			count = count + (5*multiply);
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Seis")) {

			StartCoroutine( ShowAndHide(other.gameObject, 20.0f) ); // 1 second
			count = count + (6*multiply);
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Siete")) {

			StartCoroutine( ShowAndHide(other.gameObject, 20.0f) ); // 1 second
			count = count + (7*multiply);
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Ocho")) {

			StartCoroutine( ShowAndHide(other.gameObject, 20.0f) ); // 1 second
			count = count + (8*multiply);
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Nueve")) {

			StartCoroutine( ShowAndHide(other.gameObject, 20.0f) ); // 1 second
			count = count + (9*multiply);
//			SetCountText ();
		}
			

		if (other.gameObject.CompareTag ("Unidades")) {

			multiply = 1;
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Decenas")) {

			multiply = 10;
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Centenas")) {

			multiply = 100;
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Unidades de Mil")) {

			multiply = 1000;
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Decenas de Mil")) {

			multiply = 10000;
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Centenas de Mil")) {

			multiply = 100000;
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Unidades de Millon")) {

			multiply = 1000000;
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Decenas de Millon")) {

			multiply = 10000000;
//			SetCountText ();
		}

		if (other.gameObject.CompareTag ("Centenas de Millon")) {

			multiply = 100000000;
//			SetCountText ();
		}
			
	
	}

//	void SetCountText(){
//
//		countText.text = "Total: " + count.ToString ();
//
//	}

	void SetGoalText(){

		goalText.text = "Objetivo: " + goal.ToString ();

	}

	void getScore (){
		WriteString("Objetivo: " + goal + " " + "Resultado: " + count) ;
	}
		
	static void WriteString(string resultado)
	{
		string path = "Assets/Text/test.txt";

		//Write some text to the test.txt file
		StreamWriter writer = new StreamWriter(path, true);
		writer.WriteLine(resultado);
		writer.Close();

		//Re-import the file to update the reference in the editor
//		AssetDatabase.ImportAsset(path); 
//		TextAsset asset = Resources.Load(resultado);
//
		//Print the text from the file
//		Debug.Log(asset.text);
	}

//	static void ReadString()
//	{
//		string path = "Assets/Resources/test.txt";
//
//		//Read the text from directly from the test.txt file
//		StreamReader reader = new StreamReader(path); 
//		Debug.Log(reader.ReadToEnd());
//		reader.Close();
//	}

		

	IEnumerator ShowAndHide( GameObject go, float delay )
	{
		go.SetActive(false);
		yield return new WaitForSeconds(delay);
		go.SetActive(true);
	}


}
