﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EntrarNivel : MonoBehaviour {

	public GameObject pared;
	public GameObject pared2;
	//public GameObject cubo;
	//public GameObject otraPuerta;
	public GameObject opcion1;
	public GameObject opcion2;
	public GameObject opcion3;
	private int random;
	private string elegida;
	public Text texto1;
	public Text texto2;
	public Text texto3;
	public Text pregunta;

	//private bool seMueve=false;
	// Use this for initialization

	void GenerarPregunta(){
		string[] uno=new string[4];
		string[] dos=new string[4] ;
		string[] tres=new string[4];

		if (this.tag == "NivelAlto") {
			uno [0] = "Número divisible por 5, que tenga 4 cifras y que sea divisible por 9";uno [1] = "4320";uno [2] = "5124";uno[3]= "8172" ;
			dos [0] = "Divisores de 1962";dos [1] = "2;3;6;9";dos [2] = "2;4;7;8";dos[3]= "2;3;4;6" ;
			tres [0] = "Número compuesto que no es múltiplo de 5 ni de 7 pero es múltiplo de 4";tres [1] = "12";tres [2] = "28";tres[3]= "45" ;
		}
		if (this.tag == "NivelMedio") {
			uno [0] = "Número que tiene 4 cifras distintas, que termina en 7 y es múltiplo de 9";uno [1] = "5977";uno [2] =  "2107";uno[3]= "1467" ;
			dos [0] = "Número que tiene 5 cifras distintas, que termina en 1 y es múltiplo de 11";dos [1] = "42371";dos [2] = "36211";dos[3]= "32571" ;
			tres [0] = "Número que es múltiplo de 5 pero no de 2";tres [1] = "850";tres [2] = "733";tres[3]= "835" ;
		}
		if (this.tag == "NivelBajo") {
			uno [0] = "El número 51 es múltiplo de...";uno [1] = "11";uno [2] =  "17";uno[3]= "13" ;
			dos [0] = "Número par divisor de 28";dos [1] = "6";dos [2] = "4";dos[3]= "8" ;
			tres [0] = "El número 121 es múltiplo de...";tres [1] = "17";tres [2] = "11";tres[3]= "7" ;
		}

			string[] opciones = { "1", "2", "3" };
			random = Random.Range (0, 3);
			elegida = opciones [random];

			if (elegida == "1") {
					pregunta.text = uno [0];
					texto1.text = uno [1];
					texto2.text = uno [2];
					texto3.text = uno [3];


			}

			if (elegida == "2") {
					pregunta.text = dos [0];
					texto1.text = dos [1];
					texto2.text = dos [2];
					texto3.text = dos [3];
				
			}
			if (elegida == "3") {
				
					pregunta.text = tres [0];
					texto1.text = tres [1];
					texto2.text = tres [2];
					texto3.text = tres [3];

			}


		
	}

	void OnTriggerEnter (Collider other) {
		if (other.tag == "Player") {
			//seMueve = true;

			GenerarPregunta ();

			gameObject.SetActive (false);
			if (pared) {
				if(pared.activeSelf==false){
					pared.SetActive (true);
				}
			}

			if (pared2) {
				if(pared2.activeSelf==false){
					pared2.SetActive (true);
				}
			}

			if (opcion1) {
				if (opcion1.activeSelf == false) {
					opcion1.SetActive (true);
					for (int i = 0; i < opcion1.transform.childCount; ++i)
					{
						opcion1.transform.GetChild(i).gameObject.SetActive(true);
					}
				}
			}
			if (opcion2) {
				if (opcion2.activeSelf == false) {
					opcion2.SetActive (true);
					for (int i = 0; i < opcion2.transform.childCount; ++i)
					{
						opcion2.transform.GetChild(i).gameObject.SetActive(true);
					}
				}
			}
			if (opcion3) {
				if (opcion3.activeSelf == false) {
					opcion3.SetActive (true);
					for (int i = 0; i < opcion3.transform.childCount; ++i)
					{
						opcion3.transform.GetChild(i).gameObject.SetActive(true);
					}
				}
			}

		}
	}
}
