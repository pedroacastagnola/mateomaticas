﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;
using System.IO;

public class PlayerController2 : MonoBehaviour {

	public GameObject cuadroLibro;
	public GameObject cuadroEspadas;
	public GameObject cuadroPiramide;
	public GameObject cuadroPinturas;
	public GameObject cofre;
	bool entro1 = false;
	bool entro2 = false;
	bool entro3 = false;
	bool entro4 = false;
	private Rect windowRect = new Rect ((Screen.width - 250)/2, (Screen.height - 200)/2, 250, 200);
	private bool show = false;
	private bool show2 = false;
	public Texture image;

	void Start(){
		
		ocultarCuadro(cuadroLibro);
		ocultarCuadro (cuadroEspadas);
		ocultarCuadro (cuadroPiramide);
		ocultarCuadro (cuadroPinturas);
		ocultarCuadro (cofre);
		Open ();
	}


	void FixedUpdate(){


	}

	void OnGUI () 
	{
		if(show)
			windowRect = GUI.Window (0, windowRect, DialogWindow, "Ángulos y segmentos");
		if (show2) {
			//var estilo = GUI.skin.GetStyle ("Window");
			//estilo.normal.background.
			GUI.backgroundColor=Color.green;
			windowRect = new Rect ((Screen.width - 400)/2, (Screen.height - 100)/2, 400, 100);
			windowRect = GUI.Window (0, windowRect, DialogWindow2, "¡Fin del juego!");

		}
	}	

	// This is the actual window.
	void DialogWindow (int windowID)
	{
		GUI.Label (new Rect (5, 40, windowRect.width - 10, 100), "Encontrá y jugá a los cuatro minijuegos para desbloquear el cofre.");

		if(GUI.Button(new Rect(10,150, windowRect.width - 20, 30), "¡Jugar!"))
		{
			show = false;
		}

	}

	void DialogWindow2 (int windowID)
	{
//		var estilo = GUI.skin.GetStyle ("Label");
//		estilo.fontSize=1;
		GUI.Label (new Rect (5, 40, windowRect.width - 10, 100), "¡Felicitaciones! Completaste todos los niveles. ¡Esperamos que te hayas divertido!");

	}

	// To open the dialogue from outside of the script.
	public void Open()
	{
		show = true;
	}
	public void Open2()
	{
		show2 = true;
	}


	public void WriteString(string resultado)
	{
		string path = "Assets/Text/test2.txt";

		StreamWriter writer = new StreamWriter(path, true);
		writer.WriteLine(resultado);
		writer.Close();
	}

	public void ocultarCuadro (GameObject cuadro){
		cuadro.SetActive (false);
	}

	public void mostrarCuadro (GameObject cuadro){
		cuadro.SetActive (true);
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.CompareTag ("Libro")) {
			mostrarCuadro (cuadroLibro);
			entro1 = true;
		}
		if (other.gameObject.CompareTag ("Espadas")) {
			mostrarCuadro (cuadroEspadas);
			entro2 = true;
		}
		if (other.gameObject.CompareTag ("Piramide")) {
			mostrarCuadro (cuadroPiramide);
			entro3 = true;
		}
		if (other.gameObject.CompareTag ("Pinturas")) {
			mostrarCuadro (cuadroPinturas);
			entro4 = true;
		}
		if (entro1 && entro2 && entro3 && entro4) {
			mostrarCuadro (cofre);
		}
		if (other.gameObject.CompareTag ("Cofre")) {
			WriteString ("Juego Terminado");
			ocultarCuadro (cofre);
		}
		if (other.gameObject.CompareTag ("Reinicio")) {
			transform.position = new Vector3(0.0f, 0.0f, 0.0f);
			WriteString ("Se cayó");
			Start();
		}
		if (other.tag == "Cofre") {
			Open2();
		}
	}

}
