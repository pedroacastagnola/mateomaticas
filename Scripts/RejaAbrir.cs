﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RejaAbrir : MonoBehaviour {

	public GameObject antorcha;
	public GameObject reja;
	//public Text texto;
	private int vel=90;
	private Vector3 direccion;
	public GameObject otraOpcion;
	public GameObject otraOpcion2;

	void OnTriggerStay(Collider other) {
		if (other.tag == "Player") {
			//Debug.Log ("entro");
			//texto.enabled=true;
			if (Input.GetKeyDown ("o")) {
				//angulo = 90;
				direccion = Vector3.up;
				reja.transform.Rotate (direccion * vel);
				antorcha.transform.Rotate (Vector3.right * 45);
				gameObject.SetActive(false);
				if (otraOpcion) {
					if (otraOpcion.activeSelf == true) {
						otraOpcion.SetActive (false);
					}
				}
				if (otraOpcion2) {
					if (otraOpcion2.activeSelf == true) {
						otraOpcion2.SetActive (false);
					}
				}
				//texto.enabled=false;
			}

			//	Debug.Log ("abre");
			//puerta.transform.rotation = Quaternion.Lerp (puerta.transform.rotation, Quaternion.Euler (0, (-120), 0), 30 * Time.deltaTime);
			//puerta.transform.Rotate(Vector3.up,500*Time.deltaTime);

		}
	}
}
