# Mateomáticas
_Mateomáticas_ is a third-person 3D Edutainment video game. The player must accomplish 4 Maths challenges to win the game.
### Number composition yard
<img src="Imagenes/Juego1.png">

### Ecuations dungeon
<img src="Imagenes/Juego2.png">

### Maze of multiples and divisors
<img src="Imagenes/Juego3.png">

### Geometry playground
<img src="Imagenes/Juego4.png">

My colleague and I implemented a new evaluation method in a primary school classroom with the objective of comparing it with the the traditional method.

The main goals were the following:

- **Student's comfort**. There can be external causes that may affect their performance (like anxiety, worry, fear, etc).

- Help them understand the purpose of exams. We considered an **approach based on achievements**: the exams are useful for learning.

- **Innovate** in terms of teaching and learning.

- Find a more efficient way to **include children with special needs** in classrooms.


# COPYRIGHT 2018

Design and development by Pedro Agustín Castagnola and Martin Echeguren
